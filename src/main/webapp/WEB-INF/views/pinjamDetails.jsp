<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<style>
.table-borderless td, .table-borderless th {
	border: none !important;
}
</style>
<body>

	<br>
	<div class="container">

		<h2>Pinjam List</h2>

		<button class="btn btn-success" onClick="btnAdd()">Add</button>
		<br> <br>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">id</th>
				<th class="text-center">Nama Peminjam</th>
				<th class="text-center">Tanggal</th>
				<th class="text-center">Judul Buku</th>
				<th colspan=2 class="text-center">Action</th>
			</tr>
			<c:if test="${!empty listOfPinjam}">
				<c:forEach items="${listOfPinjam}" var="pinjam">
					<tr>
						<td>${pinjam.id}</td>
						<td>${pinjam.nama}</td>
						<td>${pinjam.tgl}</td>
						<td>${pinjam.buku.judul}</td>
						<td><a href="" onclick="btnEdit(${pinjam.id})">Edit</a></td>
						<td><a href="<c:url value='/deletePinjam/${pinjam.id}' />">Delete</a></td>
					</tr>
				</c:forEach>
			</c:if>
			<c:if test="${empty listOfPinjam}">
				<tr>
					<td colspan=4 class="text-center">Data Tidak Ada</td>
				</tr>
			</c:if>
		</table>

	</div>

	<div class="modal fade" id="modalAdd" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content" id="modal-content">
				<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>
				Add</div>
				<div class="modal-body">
					<form >
					    <input type="hidden" id="idPinjam">
					    <input type="hidden" id="isAdd">
						<div class="form-group">
							<label>Tanggal</label> <input type="text" id="tglPinjam"
								placeholder="input tanggal" class="form-control">
						</div>
						<div class="form-group">
							<label>Nama Peminjam</label> <input type="text" id="namaPinjam"
								placeholder="input Nama Peminjam" class="form-control">
						</div>
						<div class="form-group">
							<label>Buku</label> <select id="bukuList" class="form-control">
								<option>--Pilih Pinjam--</option>
							</select>
						</div>
						<div class="form-group">
							
						</div>
					</form>
					<button class="btn btn-success" onclick="submit()">Submitt</button>
				</div>
			</div>

		</div>
	</div>


</body>
<script>
	$(document).ready(function() {
		
		comboBuku();
	});
	function cleanModal(){
		$("#idPinjam").val("");
		$("#tglPinjam").val("");
		$("#customerList").val("");
		$("#bukuList").val("");
		$("#isAdd").val("");
	}
	function comboBuku() {
		$.ajax({
			url : "${pageContext.request.contextPath}/getBukuList",
			type : "get",
			dataType : "json",
			success : function(response) {
				for (var i = 0; i < response.length; i++){
					$('#bukuList').append(
							"<option value="+response[i].id+">"
									+ response[i].judul + "</option>")}
			},
			error : function(textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}
	function showModal() {
		$('#modalAdd').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalAdd').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function btnAdd() {
		cleanModal();
		$("#idPinjam").val(0);
		$("#isAdd").val(true);
		showModal();
	}
	function btnEdit(id) {
		$.ajax({
			url : "${pageContext.request.contextPath}/getPinjam/"+id,
			type : "get",
			dataType : "json",
			success : function(response) {
				cleanModal();
				$("#idPinjam").val(response.id);
				$("#tglPinjam").val(response.tgl);
				$("#namaPinjam").val(response.nama);
				$("#bukuList").val(response.buku.id);
				$("#isAdd").val(false);
				showModal();
			},
			error : function(e) {
				alert("error Edit");
			}
		});
		
	}
	function submit(){
		$.ajax({
			url : "${pageContext.request.contextPath}/addPinjam",
			type : "post",
			dataType : "json",
			data:{
				id:$("#idPinjam").val(),
				isAdd:$("#isAdd").val(),
				tglPinjam:$("#tglPinjam").val(),
				buku:$("#bukuList").val(),
				namaPinjam:$("#namaPinjam").val()
			},
			success : function(response) {
				var id = $("#idPinjam").val();
				if(id == null || id == 0){
					alert("Data Berhasil Ditambahkan");
				}else{
					alert("Data Berhasil Diupdate");
					
				}
				window.location.replace('${pageContext.request.contextPath}/getAllPinjam');
				
			},
			error : function(e) {
				alert("Error");
			}
		});
	}
</script>
</html>