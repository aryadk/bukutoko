<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<style>
	.table-borderless td,
.table-borderless th {
    border: none !important;
}
</style>
<body>
<div style="padding:2%">
<a class="pull-right" style="cursor: pointer;" onClick="location.reload();">X</a>
</div>
	<form:form method="post" modelAttribute="buku"
		action="/SpringMVCHibernateCRUDExample/addBuku">
		<table class = "table table-borderless" style="margin: 4%; width: 90%; ">
			<tr class = "form-group">
				<form:hidden path="id" />
				<td><form:label path="judul" >Judul:</form:label></td>
				<td><form:input path="judul" class="form-control" size="30" maxlength="30" ></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="harga">Harga:</form:label></td>
				<td><form:input path="harga" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<form:hidden path="detailBuku.id" />
				<td><form:label path="detailBuku.pengarang">pengarang:</form:label></td>
				<td><form:input path="detailBuku.pengarang" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="detailBuku.penerbit">penerbit:</form:label></td>
				<td><form:input path="detailBuku.penerbit" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="detailBuku.tahun_terbit">tahun terbit:</form:label></td>
				<td><form:input path="detailBuku.tahun_terbit" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr>
				<td colspan="2" class="text-right">
				<input type="submit" class="btn btn-info " />
				
				</td>
			</tr>
		</table>
	</form:form>
	
</body>
</html>