<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<style>
.table-borderless td, .table-borderless th {
	border: none !important;
}
</style>
<body>

	<br>
	<div class="container">

		<h2>Customer Listhhh</h2>

		<button class="btn btn-primary pull-right" onClick="btnAdd()">Add</button>
		<button class="btn btn-success" onClick="btnExport()">Export
			to excel</button>
		<button class="btn btn-warning" onClick="showModal2()">Import
			from excel</button>
		<br> <br>
		<table class="table table-bordered">
			<tr>
				<th>id</th>
				<th>Nama</th>
				<th>Nama Lengkap</th>
				<th>Alamat</th>
				<th>Umur</th>
				<th>Status</th>
				<th>Warga Negara</th>
				<th colspan=2 class="text-center">Action</th>
			</tr>
			<c:if test="${!empty listOfCustomer}">
				<c:forEach items="${listOfCustomer}" var="customer">
					<tr>
						<td>${customer.id}</td>
						<td>${customer.nama}</td>
						<td>${customer.namaPanjang}</td>
						<td>${customer.alamat}</td>
						<td>${customer.umur}</td>
						<td>${customer.status}</td>
						<td>${customer.wargaNegara}</td>
						<td><a href="#" onclick="btnEdit(${customer.id})">Edit</a></td>
						<td><a href="#" onclick="btnDelete(${customer.id})">Delete</a></td>
					</tr>
				</c:forEach>
			</c:if>
			<c:if test="${empty listOfCustomer}">
				<tr>
					<td colspan=8 class="text-center">Data Tidak Ada</td>
				</tr>
			</c:if>
		</table>

	</div>

	<div class="modal fade" id="modalAdd" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content" id="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					Add
				</div>
				<div class="modal-body">
					<form>
						<input type="hidden" id="id"> <input type="hidden"
							id="isAdd">
						<div class="form-group">
							<label>Nama Peminjam</label> <input type="text" id="nama"
								class="form-control">
						</div>
						<div class="form-group">
							<label>Nama Lengkap Peminjam</label> <input type="text"
								id="namaPanjang" class="form-control">
						</div>
						<div class="form-group">
							<label>Alamat</label> <input type="text" id="alamat"
								class="form-control">
						</div>
						<div class="form-group">
							<label>Umur</label> <input type="number" id="umur"
								class="form-control">
						</div>
						<div class="form-group">
							<label>Status</label> <input type="text" id="status"
								class="form-control">
						</div>
						<div class="form-group">
							<label>Warga Negara</label> <input type="text" id="wargaNegara"
								class="form-control">
						</div>
					</form>
					
				</div>
				<div class="modal-footer">
					<button class="btn btn-success pull-right" onclick="submit()">Submit</button>
				</div>
			</div>

		</div>
	</div>
	
	<div class="modal fade" id="modalImport" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content" id="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					Import Data
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label>File</label> 
							<input type="file" id="file" class="form-control-file">
						</div >
							
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success pull-right" onclick="btnImport()">Submit</button>
				</div>
			</div>

		</div>
	</div>


</body>
<script>
	$(document).ready(function() {
	});
	function showModal() {
		$('#modalAdd').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalAdd').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function showModal2() {
		$('#modalImport').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalImport').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function btnAdd() {
		$("#id").val(0);
		$("#isAdd").val(true);
		showModal();
	}
	function btnEdit(id) {
		$.ajax({
			url : "${pageContext.request.contextPath}/getCustomer/"+id,
			type : "get",
			dataType : "json",
			success : function(response) {
				$("#id").val(response.id);
				$("#nama").val(response.nama);
				$("#namaPanjang").val(response.namaPanjang);
				$("#alamat").val(response.alamat);
				$("#umur").val(response.umur);
				$("#status").val(response.status);
				$("#wargaNegara").val(response.wargaNegara);
				$("#isAdd").val(false);
				showModal();
			},
			error : function(e) {
				alert("Error");
			}
		});
		
	}
	function submit(){
		$.ajax({
			url : "${pageContext.request.contextPath}/addCustomer",
			type : "post",
			dataType : "json",
			data:{
				id:$("#id").val(),
				isAdd:$("#isAdd").val(),
				nama:$("#nama").val(),
				namaPanjang: $("#namaPanjang").val(),
				alamat: $("#alamat").val(),
				umur: $("#umur").val(),
				status: $("#status").val(),
				wargaNegara: $("#wargaNegara").val(),
			},
			success : function(response) {
				var id = $("#id").val();
				if(id == null || id == 0){
					alert("Data Berhasil Ditambahkan");
				}else{
					alert("Data Berhasil Diupdate");
					
				}
				window.location.replace('${pageContext.request.contextPath}/getAllCustomer');
				
			},
			error : function(e) {
				alert("Error");
			}
		});
	}
	function btnExport(){
		$.ajax({
			url : "${pageContext.request.contextPath}/exportExcel",
			type : "post",
			dataType : "json",
			success : function(response) {
				alert("Success writen");
				window.location.replace('${pageContext.request.contextPath}/getAllCustomer');
				
			},
			error : function(e) {
				alert("Error log");
			}
		});
	}
	function btnImport(){
		var file = document.getElementById("file").files[0].name;
		$.ajax({
			url : "${pageContext.request.contextPath}/importExcel",
			type : "post",
			dataType : "json",
			data:{
					file:file
			},
			success : function(response) {
				alert(response.message);
				window.location.replace('${pageContext.request.contextPath}/getAllCustomer');
				
			},
			error : function(e) {
				alert("Error log");
			}
		});
	}
	function btnDelete(id){
		var con =  confirm("Apakah Anda yakin menghapus data ini?");
		if(con==true){
		$.ajax({
			url : "${pageContext.request.contextPath}/deleteCustomer",
			type : "post",
			dataType : "json",
			data:{
					id:id
			},
			success : function(response) {
				alert("Data Berhasil Dihapus");
				window.location.replace('${pageContext.request.contextPath}/getAllCustomer');
				
			},
			error : function(e) {
				alert("Error log");
			}
		});
		}
	}
</script>
</html>