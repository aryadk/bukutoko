<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>    
</head>
<body>
       <div style="padding:2%">
<a class="pull-right" style="cursor: pointer;" onClick="location.reload();">X</a>
</div><br>
          <table class="table" style="margin:4%;width:90%">
	          <tr>
	          <td>ID :</td>
	          <td> ${buku.id}</td>
	          </tr>
	          <tr>
	          <td>Judul :</td>
	          <td>${buku.judul}</td>
	          </tr>
	          <tr>
	          <td>Harga :</td>
	          <td>${buku.harga}</td>
	          <tr>
	          <td>Pengarang :</td>
	          <td>${buku.detailBuku.pengarang}</td>
	          </tr>
	          <tr>
	          <td>Penerbit :</td>
	          <td>${buku.detailBuku.penerbit}</td>
	          </tr>
	          <td>Tahun Terbit :</td>
	          <td>${buku.detailBuku.tahun_terbit}</td>
	          </tr>
	          </tr>
          </table>
</body>
</html>