<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>    
</head>
<style>
	.table-borderless td,
.table-borderless th {
    border: none !important;
}
</style>
<body>

<br>
<div class="container">

<h2> Buku List </h2>

	<button class="btn btn-success pull-right" data-toggle="modal" data-target="#add">Add</button><br><br>
	 <table class = "table table-bordered">
	<tr>
		<th width ="80" class = "text-center">id</th>
		<th width="800" class = "text-center">Judul</th>
		<th width="300"class = "text-center">Harga</th>
		<th colspan=3 class = "text-center">Action</th>
	</tr>
	<c:if test = "${!empty listOfBuku}">
	<c:forEach items="${listOfBuku}" var="buku">
		<tr>
			<td>${buku.id}</td>
			<td>${buku.judul}</td>
			<td>${buku.harga}</td>
			<td><a href="<c:url value='/detailBuku/${buku.id}' />" data-toggle="modal" data-target="#detail">Detail</a></td>
			<td><a href="<c:url value='/updateBuku/${buku.id}' />" data-toggle="modal" data-target="#edit">Edit</a></td>
			<td><a href="<c:url value='/deleteBuku/${buku.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</c:if>
	<c:if test = "${empty listOfBuku}">
	<tr>
		<td colspan=4 class="text-center">Data Tidak Ada</td>
	</tr>
	</c:if>
	</table>
<br>

<div class="modal fade" id="detail" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="edit" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
      <div style="padding:2%">
<a class="pull-right" style="cursor: pointer;" onClick="location.reload();">X</a>
</div>
       <form:form method="post" modelAttribute="buku"
		action="/SpringMVCHibernateCRUDExample/addBuku">
		<table class = "table table-borderless" style="margin: 4%; width: 90%; ">
			<tr class = "form-group">
				<form:hidden path="id" />
				<form:hidden path="detailBuku.id" />
				<td><form:label path="judul" >Judul:</form:label></td>
				<td><form:input path="judul" class="form-control" size="30" maxlength="30" ></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="harga">Harga:</form:label></td>
				<td><form:input path="harga" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="detailBuku.pengarang">pengarang:</form:label></td>
				<td><form:input path="detailBuku.pengarang" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="detailBuku.penerbit">penerbit:</form:label></td>
				<td><form:input path="detailBuku.penerbit" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr class = "form-group">
				<td><form:label path="detailBuku.tahun_terbit">tahun terbit:</form:label></td>
				<td><form:input path="detailBuku.tahun_terbit" size="30" maxlength="30" class="form-control"></form:input></td>
			</tr>
			<tr>
				<td colspan="2" class="text-right">
				<input type="submit" class="btn btn-info " />
				
				</td>
			</tr>
		</table>
	</form:form>
      </div>
      
    </div>
  </div>

</body>
</html>