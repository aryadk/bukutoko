<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<style>
.table-borderless td, .table-borderless th {
	border: none !important;
}
</style>
<body>

	<br>
	<div class="container">

		<h2>Film List</h2>

		<button class="btn btn-primary pull-right" onClick="btnAdd()">Add</button>
		<button class="btn btn-success" onClick="btnExport()">Export
			to excel</button>
		<button class="btn btn-warning" onClick="showModal2()">Import
			from excel</button>
		<br> <br>
		<table class="table table-bordered">
			<tr>
				<th>id</th>
				<th>Judul</th>
				<th>Durasi</th>
				<th>Aktor</th>
				<th colspan=2 class="text-center">Action</th>
			</tr>
			<c:if test="${!empty listOfFilm}">
				<c:forEach items="${listOfFilm}" var="film">
					<tr>
						<td>${film.id}</td>
						<td>${film.judulFilm}</td>
						<td>${film.durasi}Jam</td>
						<td><a href="#" onclick="listAktor(${film.id})">List
								Aktor</a></td>
						<td><a href="#" onclick="btnEdit(${film.id})">Edit</a></td>
						<td><a href="#" onclick="btnDelete(${film.id})">Delete</a></td>
					</tr>
				</c:forEach>
			</c:if>
			<c:if test="${empty listOfFilm}">
				<tr>
					<td colspan=8 class="text-center">Data Tidak Ada</td>
				</tr>
			</c:if>
		</table>

	</div>
	<div class="modal fade" id="modalList" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content" id="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					List Aktor
				</div>
				<div class="modal-body">
					<table class="table">
						<thead></thead>
						<tbody id="tb_aktor"></tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="modalAdd" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content" id="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					Add Film
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Judul</label> <input class="form-control" type="text"
							id="judulFilm">
					</div>
					<div class="form-group">
						<label>Durasi</label> <input class="form-control" type="number"
							id="durasi">
					</div>
					<label>Aktor</label>
					<table class="table">
						<thead>
						</thead>
						<tbody id="tb_listAktor"></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" onclick="submit()" >Submit</button>
				</div>

			</div>

		</div>
	</div>


</body>
<script>
	$(document).ready(function() {
	});
	function showModal() {
		$('#modalList').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalList').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function showModal2() {
		$('#modalAdd').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalAdd').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function showModal3() {
		$('#modalAktor').modal({
			backdrop : 'static',
			keyboard : true,
			show : true
		});

		$('#modalAktor').on('shown.bs.modal', function() {
			$('#name').focus();
		});
	}
	function getCheckedData(key) {
		var $chked = $('input[id=chk]:checked');
		var ids = [];
		$chked.each(function() {
			ids.push($(this).attr(key));
		});
		return ids;
	}
	function btnAdd(){
		setAktor();
		showModal2();
	}
	function submit(){
		var index = getCheckedData('data-index');
		var id = 0;
		$.ajax({
			url : "${pageContext.request.contextPath}/addFilm",
			type : "post",
			dataType : "json",
			data:{
					id : id,
					indexAktor : index.toString(),
					judulFilm : $("#judulFilm").val(),
					durasi : $("#durasi").val(),
			},
			success : function(response) {
				alert("Data Berhasil Ditambah");
				window.location.replace('${pageContext.request.contextPath}/getAllFilm');
			},
			error : function(e) {
				alert("Error Add");
			}
		}); 
	}
	function listAktor(id){
		$('#tb_aktor').empty();
		$.ajax({
			url : "${pageContext.request.contextPath}/getAktor",
			type : "get",
			dataType : "json",
			data:{
					id:id
			},
			success : function(response) {
				if(response.length > 0){
					for(var i =0;i<response.length;i++){	
						var no = i+1;
						$("#tb_aktor").append(
								"<tr>"
								+"<td>"+no+"</td>"
								+"<td>"+response[i]+"</td>"
								+"</tr>"
						)
					}
				}else{
					$("#tb_aktor").append("<p>Data tdak ada</p>")
				}
				showModal();
			},
			error : function(e) {
				alert("Error log");
			}
		});
	}
	function btnDelete(id){
		var con =  confirm("Apakah Anda yakin menghapus data ini?");
		if(con==true){
		$.ajax({
			url : "${pageContext.request.contextPath}/deleteFilm",
			type : "post",
			dataType : "json",
			data:{
					id:id
			},
			success : function(response) {
				alert("Data Berhasil Dihapus");
				window.location.replace('${pageContext.request.contextPath}/getAllFilm');
				
			},
			error : function(e) {
				alert("Error log");
			}
		});
		}
	}
	function setAktor(){
		$("#tb_listAktor").empty();
		$.ajax({
			url : "${pageContext.request.contextPath}/getListOfAktor",
			type : "get",
			dataType : "json",
			success : function(response) {
				if(response.length > 0){
					for(var i =0;i<response.length;i++){	
						var no = i+1;
						$("#tb_listAktor").append(
								"<tr>"
								+"<td>"+no+"</td>"
								+"<td>"+response[i].namaAktor+"</td>"
								+"<td>"
								+"<input class='checkboxes' type='checkbox' id='chk' name='chk' data-index='"+i+"' data-id='"+response[i].id+"'>"
								+"</td>"
								+"</tr>"
						)
					}
				}else{
					$("#tb__listAktor").append("<p>Data tidak ada</p>")
				}
			},
			error : function(e) {
				alert("Error log");
			}
		});
	}
</script>
</html>