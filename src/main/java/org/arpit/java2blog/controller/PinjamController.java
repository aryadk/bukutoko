package org.arpit.java2blog.controller;

import java.util.List;

import org.arpit.java2blog.model.Buku;
import org.arpit.java2blog.model.Customer;
import org.arpit.java2blog.model.Pinjam;
import org.arpit.java2blog.service.BukuService;
import org.arpit.java2blog.service.CustomerService;
import org.arpit.java2blog.service.PinjamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
public class PinjamController {
	@Autowired
	PinjamService pinjamService;
	
	@Autowired
	BukuService bukuService;
	
	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value = "/getAllPinjam",method = RequestMethod.GET)
	public String getBuku(Model model) {
    	List<Pinjam> listOfPinjam = pinjamService.getAllPinjam();
    	
		model.addAttribute("pinjam", new Pinjam());
		model.addAttribute("listOfPinjam",listOfPinjam);
		return "pinjamDetails";
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPinjam/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
	public String getPinjamId(@PathVariable("id") int id){
		Gson gson = new Gson();
		Pinjam pinjam = pinjamService.getPinjam(id);
		return gson.toJson(pinjam);
	}
	@ResponseBody
	@RequestMapping(value = "/getBukuList",method = RequestMethod.GET, headers = "Accept=application/json")
	public String getBukuList(){
		Gson gson = new Gson();
		List<Buku> buku = bukuService.getAllBuku();
		return gson.toJson(buku);
	}
	
	@ResponseBody
	@RequestMapping(value = "/addPinjam",method = RequestMethod.POST, headers = "Accept=application/json")
	public Pinjam addPinjam(@RequestParam(value="id")Integer id,
			@RequestParam(value="tglPinjam")String tgl,
			@RequestParam(value="namaPinjam")String nama,
			@RequestParam(value="buku")Integer idbuku,
			@RequestParam(value="isAdd")boolean isAdd){
		Pinjam pinjam = new Pinjam();
		pinjam.setTgl(tgl);
		pinjam.setBuku(bukuService.getBuku(idbuku));
		pinjam.setNama(nama);
		pinjam.setId(id);
		if(pinjam.getId() == 0) {
			pinjamService.addPinjam(pinjam);
		}else {
			
			pinjamService.updatePinjam(pinjam);
		}
		
		return pinjam;
	}
	
}
