package org.arpit.java2blog.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.arpit.java2blog.dao.ResponCustomer;
import org.arpit.java2blog.model.Customer;
import org.arpit.java2blog.service.CustomerService;
import org.arpit.java2blog.service.PinjamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
public class CustomerController {
	@Autowired
	CustomerService customerService;
	@Autowired
	PinjamService pinjamService;

	public List<Customer> CustomerList() {
		List<Customer> listOfCustomer = customerService.getAllCustomer();
		return listOfCustomer;
	}

	@RequestMapping(value = "/getAllCustomer", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getBuku(Model model) {
		List<Customer> listOfCustomer = customerService.getAllCustomer();
		model.addAttribute("customer", new Customer());
		model.addAttribute("listOfCustomer", listOfCustomer);
		return "customerDetails";
	}

	@ResponseBody
	@RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET)
	public String getCustomerId(@PathVariable("id") int id) {
		Gson gson = new Gson();
		Customer customer = customerService.getCustomer(id);
		return gson.toJson(customer);
	}

	@ResponseBody
	@RequestMapping(value = "/addCustomer", method = RequestMethod.POST, headers = "Accept=application/json")
	public Customer addCustomer(@RequestParam(value = "id") Integer id, @RequestParam(value = "nama") String nama,
			@RequestParam(value = "alamat") String alamat, @RequestParam(value = "umur") Integer umur,
			@RequestParam(value = "namaPanjang") String namaPanjang, @RequestParam(value = "status") String status,
			@RequestParam(value = "wargaNegara") String wargaNegara, @RequestParam(value = "isAdd") boolean isAdd) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setNama(nama);
		customer.setAlamat(alamat);
		customer.setNamaPanjang(namaPanjang);
		customer.setUmur(umur);
		customer.setStatus(status);
		customer.setWargaNegara(wargaNegara);
		if (customer.getId() == 0) {
			customerService.addCustomer(customer);
		} else {

			customerService.updateCustomer(customer);
		}

		return customer;
	}

	@ResponseBody
	@RequestMapping(value = "/deleteCustomer", method = RequestMethod.POST, headers = "Accept=application/json")
	public Customer deleteCustomer(@RequestParam(value = "id") Integer id) {
		customerService.deleteCustomer(id);
		Customer customer = new Customer();
		return customer;
	}

	@ResponseBody
	@RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
	public Customer exportExcel() throws IOException {
		List<Customer> customers = customerService.getAllCustomer();
		String[] fields = { "Id", "Nama", "Nama Panjang", "Alamat", "Umur", "Status", "Warga Negara" };
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet("customer");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		XSSFRow row = spreadsheet.createRow(1);
		XSSFCell cell;
		for (int i = 0; i < fields.length; i++) {
			cell = row.createCell(i + 1);
			cell.setCellValue(fields[i]);
			cell.setCellStyle(headerCellStyle);
		}
		int i = 0;
		while (i < customers.size()) {
			row = spreadsheet.createRow(i + 2);
			cell = row.createCell(1);
			cell.setCellValue(customers.get(i).getId());
			cell = row.createCell(2);
			cell.setCellValue(customers.get(i).getNama());
			cell = row.createCell(3);
			cell.setCellValue(customers.get(i).getNamaPanjang());
			cell = row.createCell(4);
			cell.setCellValue(customers.get(i).getAlamat());
			cell = row.createCell(5);
			cell.setCellValue(customers.get(i).getUmur());
			cell = row.createCell(6);
			cell.setCellValue(customers.get(i).getStatus());
			cell = row.createCell(7);
			cell.setCellValue(customers.get(i).getWargaNegara());
			i++;
		}

		for (i = 0; i < fields.length; i++) {
			spreadsheet.autoSizeColumn(i + 2);
		}
		FileOutputStream out = new FileOutputStream(
				new File("C:\\Users\\PTBSP\\Documents\\new 25\\project\\Customers.xlsx"));
		workbook.write(out);
		out.close();
		workbook.close();
		Customer customer = new Customer();
		return customer;

	}

	@ResponseBody
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public ResponCustomer importExcel(@RequestParam(value = "file") String file) throws IOException {

		ResponCustomer respon = customerService.importCustomer(file);
		List<Customer> customers = respon.getList();
		Iterator<Customer> customersIter = customers.iterator();
		if (respon.isStatus() == true) {
			while (customersIter.hasNext()) {
				customerService.addCustomer(customersIter.next());
			}
		}
		return respon;
	}

}
