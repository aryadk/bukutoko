package org.arpit.java2blog.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.arpit.java2blog.service.AktorService;
import org.arpit.java2blog.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
public class FilmController {
	@Autowired
	FilmService filmService;

	@Autowired
	AktorService aktorService;

	public List<Film> FilmList() {
		List<Film> listOfFilm = filmService.getAllFilm();
		return listOfFilm;
	}

	@RequestMapping(value = "/getAllFilm", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getBuku(Model model) {
		List<Film> listOfFilm = filmService.getAllFilm();
		model.addAttribute("film", new Film());
		model.addAttribute("listOfFilm", listOfFilm);
		return "filmDetails";
	}

	@ResponseBody
	@RequestMapping(value = "/getFilm/{id}", method = RequestMethod.GET)
	public String getFilmId(@PathVariable("id") int id) {
		Gson gson = new Gson();
		Film film = filmService.getFilm(id);
		return gson.toJson(film);
	}

	@ResponseBody
	@RequestMapping(value = "/getAktor", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<String> getAktor(@RequestParam(value = "id") Integer id) {
		List<String> actors = new ArrayList<String>();
		Film film = filmService.getFilm(id);
		Set<Aktor> listAktor = film.getAktor();
		for (Aktor aktorList : listAktor) {
			/*
			 * Aktor aktor = new Aktor(); aktor.setId(aktorList.getId());
			 * aktor.setNamaAktor(aktorList.getNamaAktor());
			 * aktor.setUmur(aktorList.getUmur()); aktor.setFilm(aktorList.getFilm());
			 */
			actors.add(aktorList.getNamaAktor());
		}
		return actors;
	}

	@ResponseBody
	@RequestMapping(value = "/getListOfAktor", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getListOfAktor() {
		Gson gson = new Gson();
		List<Aktor> listOfAktor = aktorService.getAllAktor();
		return gson.toJson(listOfAktor);
			
		
	}

	@ResponseBody
	@RequestMapping(value = "/addFilm", method = RequestMethod.POST, headers = "Accept=application/json")
	public Film addFilm(@RequestParam(value = "id") Integer id,
						@RequestParam(value = "indexAktor") String[] indexAktor,
						@RequestParam(value = "judulFilm") String judulFilm,
						@RequestParam(value = "durasi") Integer durasi) {
		Set<Aktor> listAktor = new HashSet<>();
		List<Aktor> listOfAktor = aktorService.getAllAktor();
		for(int i = 0; i<indexAktor.length;i++) {
			int indexes = Integer.parseInt(indexAktor[i]);
			listAktor.add(listOfAktor.get(indexes));
		}
		Film film = new Film();
		film.setId(id);
		film.setJudulFilm(judulFilm);
		film.setDurasi(durasi);
		film.setAktor(listAktor);
		if (film.getId() == 0) {
			filmService.addFilm(film);
		} else {

			filmService.updateFilm(film);
		}

		return film;
	}

	@ResponseBody
	@RequestMapping(value = "/deleteFilm", method = RequestMethod.POST, headers = "Accept=application/json")
	public Film deleteFilm(@RequestParam(value = "id") Integer id) {
		filmService.deleteFilm(id);
		Film film = new Film();
		return film;
	}

}
