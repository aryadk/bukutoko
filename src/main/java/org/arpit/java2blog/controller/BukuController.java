package org.arpit.java2blog.controller;

import java.util.List;

import org.arpit.java2blog.model.Buku;
import org.arpit.java2blog.model.DetailBuku;
import org.arpit.java2blog.model.Pinjam;
import org.arpit.java2blog.service.BukuService;
import org.arpit.java2blog.service.DetailBukuService;
import org.arpit.java2blog.service.PinjamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BukuController {
	@Autowired
	BukuService bukuService;
	
	@Autowired
	DetailBukuService detbukuService;
	
	@RequestMapping(value = "/getAllBuku",method = RequestMethod.GET, headers = "Accept=application/json")
	public String getBuku(Model model) {
		List<Buku> listOfBuku = bukuService.getAllBuku();
		model.addAttribute("buku", new Buku());
		model.addAttribute("listOfBuku",listOfBuku);
		return "bukuDetails";
	}
	
	@RequestMapping(value = "/getBuku/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
	public Buku getBukuById(@PathVariable int id) {
		return bukuService.getBuku(id);
	}
	
	@RequestMapping(value = "/addBuku",method = RequestMethod.POST, headers = "Accept=application/json")
	public String addBuku(@ModelAttribute("buku") Buku buku) {
		/*DetailBuku detbuku = buku.getDetailBuku();
		buku.getDetailBuku().setId(buku.getId());
		detbuku.setBuku(buku);*/
		
		if(buku.getId() == 0) {
			bukuService.addBuku(buku);
		}else {
			bukuService.updateBuku(buku);
		}
		
		return "redirect:/getAllBuku";
	}
	
	@RequestMapping(value = "/updateBuku/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
	public String updateBuku(@PathVariable("id") int id,Model model) {
		model.addAttribute("buku",this.bukuService.getBuku(id));
		model.addAttribute("listOfBuku", this.bukuService.getAllBuku());
		return "bukuEdit";
	}
	
	@RequestMapping(value = "/detailBuku/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
	public String detailBuku(@PathVariable("id") int id,Model model) {
		model.addAttribute("buku",this.bukuService.getBuku(id));
		return "Details";
	}
	
	@RequestMapping(value = "/deleteBuku/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
	public String deleteBuku(@PathVariable int id) {
		bukuService.deleteBuku(id);
		detbukuService.deleteDetailBuku(id);
		return "redirect:/getAllBuku";
	}
}
