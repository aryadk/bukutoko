package org.arpit.java2blog.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.service.AktorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
public class AktorController {
	@Autowired
	AktorService aktorService;

	public List<Aktor> AktorList() {
		List<Aktor> listOfAktor = aktorService.getAllAktor();
		return listOfAktor;
	}

	@RequestMapping(value = "/getAllAktor", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getAktor(Model model) {
		List<Aktor> listOfAktor = aktorService.getAllAktor();
		model.addAttribute("aktor", new Aktor());
		model.addAttribute("listOfAktor", listOfAktor);
		return "aktorDetails";
	}

	@ResponseBody
	@RequestMapping(value = "/getAktorId/{id}", method = RequestMethod.GET)
	public String getAktorId(@PathVariable("id") int id) {
		Gson gson = new Gson();
		Aktor aktor = aktorService.getAktor(id);
		return gson.toJson(aktor);
	}
	

	@ResponseBody
	@RequestMapping(value = "/addAktor", method = RequestMethod.POST, headers = "Accept=application/json")
	public Aktor addAktor(@RequestParam(value = "id") Integer id) {
		Aktor aktor = new Aktor();
		if (aktor.getId() == 0) {
			aktorService.addAktor(aktor);
		} else {

			aktorService.updateAktor(aktor);
		}

		return aktor;
	}

	@ResponseBody
	@RequestMapping(value = "/deleteAktor", method = RequestMethod.POST, headers = "Accept=application/json")
	public Aktor deleteAktor(@RequestParam(value = "id") Integer id) {
		aktorService.deleteAktor(id);
		Aktor aktor = new Aktor();
		return aktor;
	}

}
