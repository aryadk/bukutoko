package org.arpit.java2blog.service;

import java.util.List;

import org.arpit.java2blog.dao.BukuDao;
import org.arpit.java2blog.model.Buku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("bukuService")
public class BukuService {
	@Autowired
	BukuDao bukuDao;
	
	@Transactional
	public List<Buku> getAllBuku(){
		return bukuDao.getAllBuku(); 
	}
	
	@Transactional
	public Buku getBuku(int id) {
		return bukuDao.getBuku(id);
	}
	
	@Transactional
	public void addBuku(Buku buku) {
		bukuDao.addBuku(buku);
	}
	
	@Transactional
	public void updateBuku(Buku buku) {
		bukuDao.updateBuku(buku);
	}
	
	@Transactional
	public void deleteBuku(int id) {
		bukuDao.deleteBuku(id);
	}
}
