package org.arpit.java2blog.service;

import java.util.List;

import org.arpit.java2blog.dao.CustomerDAO;
import org.arpit.java2blog.dao.ResponCustomer;
import org.arpit.java2blog.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
public class CustomerService {
	@Autowired
	CustomerDAO customerDao;
	
	@Transactional
	public List<Customer> getAllCustomer(){
		return customerDao.getAllCustomer();
	}
	
	@Transactional
	public Customer getCustomer(int id) {
		return customerDao.getCustomer(id);
	}
	
	@Transactional
	public void addCustomer(Customer customer) {
		customerDao.addCustomer(customer);
	}
	
	@Transactional
	public void updateCustomer(Customer customer) {
		customerDao.updateCustomer(customer);
	}
	
	@Transactional
	public ResponCustomer importCustomer(String file) {
		return customerDao.importCustomer(file);
	}
	
	@Transactional
	public void deleteCustomer(int id) {
		customerDao.deleteCustomer(id);
	}
}
