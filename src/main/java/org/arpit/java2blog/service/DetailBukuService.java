package org.arpit.java2blog.service;

import java.util.List;

import org.arpit.java2blog.dao.DetailBukuDAO;
import org.arpit.java2blog.model.DetailBuku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailBukuService {
	@Autowired
	private DetailBukuDAO detailBukuDao;
	
	public List<DetailBuku> getAllDetailBuku(){
		return detailBukuDao.getAllDetailBuku();
	}
	public DetailBuku getDetailBuku(int id) {
		return detailBukuDao.getDetailBuku(id);
	}
	public void addDetailBuku(DetailBuku detbuku) {
		detailBukuDao.addDetailBuku(detbuku);
	}
	
	public void updateDetailBuku(DetailBuku detbuku) {
		detailBukuDao.updateDetailBuku(detbuku);
	}
	
	public void deleteDetailBuku(int id) {
		detailBukuDao.deleteDetailBuku(id);
	}
}
