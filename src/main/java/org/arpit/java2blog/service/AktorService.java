package org.arpit.java2blog.service;

import java.util.List;
import java.util.Set;

import org.arpit.java2blog.dao.AktorDAO;
import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
public class AktorService {
	@Autowired
	AktorDAO aktorDao;
	
	@Transactional
	public List<Aktor> getAllAktor(){
		return aktorDao.getAllAktor();
	}
	
	@Transactional
	public Aktor getAktor(int id) {
		return aktorDao.getAktor(id);
	}
	
	
	@Transactional
	public void addAktor(Aktor aktor) {
		aktorDao.addAktor(aktor);
	}
	
	@Transactional
	public void updateAktor(Aktor aktor) {
		aktorDao.updateAktor(aktor);
	}
	
	@Transactional
	public void deleteAktor(int id) {
		aktorDao.deleteAktor(id);
	}
}
