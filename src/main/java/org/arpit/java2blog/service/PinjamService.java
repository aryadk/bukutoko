package org.arpit.java2blog.service;

import java.util.List;

import org.arpit.java2blog.dao.PinjamDAO;
import org.arpit.java2blog.model.Pinjam;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PinjamService {
	@Autowired
	PinjamDAO pinjamDao;
	
	@Transactional
	public List<Pinjam> getAllPinjam(){
		return pinjamDao.getAllPinjam();
	}
	
	@Transactional
	public Pinjam getPinjam(int id) {
		return pinjamDao.getPinjam(id);
	}
	
	@Transactional
	public List<Pinjam> getPinjamByBuku(int id) {
		return pinjamDao.getPinjamByBuku(id);
	}
	
	@Transactional
	public void addPinjam(Pinjam pinjam) {
		pinjamDao.addPinjam(pinjam);
	}
	
	@Transactional
	public void updatePinjam(Pinjam pinjam) {
		pinjamDao.updatePinjam(pinjam);
	}
	
	@Transactional
	public void deletePinjam(int id) {
		pinjamDao.deletePinjam(id);
	}
}
