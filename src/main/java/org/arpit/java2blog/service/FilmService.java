package org.arpit.java2blog.service;

import java.util.List;
import java.util.Set;

import org.arpit.java2blog.dao.FilmDAO;
import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
public class FilmService {
	@Autowired
	FilmDAO filmDao;
	
	@Transactional
	public List<Film> getAllFilm(){
		return filmDao.getAllFilm();
	}
	
	@Transactional
	public Film getFilm(int id) {
		return filmDao.getFilm(id);
	}
	
	@Transactional
	public Set<Aktor> getFilmAktor(int id) {
		return filmDao.getFilmAktor(id);
	}
	
	@Transactional
	public void addFilm(Film film) {
		filmDao.addFilm(film);
	}
	
	@Transactional
	public void updateFilm(Film film) {
		filmDao.updateFilm(film);
	}
	
	@Transactional
	public void deleteFilm(int id) {
		filmDao.deleteFilm(id);
	}
}
