package org.arpit.java2blog.dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.arpit.java2blog.model.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Customer> getAllCustomer() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Customer> customerList = session.createQuery("from Customer").list();
		return customerList;
	}

	public Customer getCustomer(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Customer customer = (Customer) session.get(Customer.class, new Integer(id));
		return customer;
	}

	public Customer addCustomer(Customer customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(customer);
		return customer;
	}

	public void updateCustomer(Customer customer) {
		this.sessionFactory.getCurrentSession().update(customer);
	}
	
	public ResponCustomer importCustomer(String file) {
		ArrayList<Customer> customers = new ArrayList<Customer>();
		String file_name = file;
		try {
			FileInputStream excelFile = new FileInputStream(
					new File("C:\\Users\\PTBSP\\Documents\\new 25\\project\\" + file_name));
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			XSSFSheet spreadsheet = workbook.getSheet("customer");
			XSSFRow row;
			XSSFCell cell;
			Iterator<Row> rowIterator = spreadsheet.rowIterator();
			int i = 0;
			while (rowIterator.hasNext()) {

				if (i > 1) {
					Customer customer = new Customer();
					row = spreadsheet.getRow(i);
					customer.setId(0);
					cell = row.getCell(2);
					customer.setNama(cell.getStringCellValue());
					cell = row.getCell(3);
					customer.setNamaPanjang(cell.getStringCellValue());
					cell = row.getCell(4);
					customer.setAlamat(cell.getStringCellValue());
					cell = row.getCell(5);
					int umur = (int) cell.getNumericCellValue();
					customer.setUmur(umur);
					cell = row.getCell(6);
					customer.setStatus(cell.getStringCellValue());
					cell = row.getCell(7);
					customer.setWargaNegara(cell.getStringCellValue());
					customers.add(customer);
					/* customerService.addCustomer(customer); */
				}
				if (i > 0) {
					rowIterator.next();
				}
				i++;

			}
			return new ResponCustomer(true,"Berhasil Import", customers);
		} catch (Exception e) {
			return new ResponCustomer(false,"Terjadi Kesalahan", customers);
		}

	}

	public void deleteCustomer(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Customer p = (Customer) session.load(Customer.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
}
