package org.arpit.java2blog.dao;

import java.util.List;

import org.arpit.java2blog.model.Buku;
import org.arpit.java2blog.model.Pinjam;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BukuDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Buku> getAllBuku(){
		Session session = this.sessionFactory.getCurrentSession();
		List<Buku> bukuList = session.createQuery("from Buku").list();
		return bukuList;
	}
	
	public Buku getBuku(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Buku buku = (Buku) session.get(Buku.class, new Integer(id));
		return buku;
	}
	
	public Buku addBuku(Buku buku) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(buku);
		return buku;
	}
	
	public void updateBuku(Buku buku) {
		this.sessionFactory.getCurrentSession().update(buku);
	}
	
	public void deleteBuku(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Buku p = (Buku) session.load(Buku.class, new Integer(id));
		if(null != p) {
			session.delete(p);
		}
	}
	
	
}
