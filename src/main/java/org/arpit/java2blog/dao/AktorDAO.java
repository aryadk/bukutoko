package org.arpit.java2blog.dao;


import java.util.List;
import java.util.Set;

import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AktorDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Aktor> getAllAktor() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Aktor> aktorList = session.createQuery("from Aktor").list();
		return aktorList;
	}

	public Aktor getAktor(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Aktor aktor = (Aktor) session.get(Aktor.class, new Integer(id));
		return aktor;
	}

	public Aktor addAktor(Aktor aktor) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(aktor);
		return aktor;
	}

	public void updateAktor(Aktor aktor) {
		this.sessionFactory.getCurrentSession().update(aktor);
	}

	public void deleteAktor(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Aktor p = (Aktor) session.load(Aktor.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
}
