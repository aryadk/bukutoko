package org.arpit.java2blog.dao;


import java.util.List;
import java.util.Set;

import org.arpit.java2blog.model.Aktor;
import org.arpit.java2blog.model.Film;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FilmDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Film> getAllFilm() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Film> filmList = session.createQuery("from Film").list();
		return filmList;
	}

	public Film getFilm(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Film film = (Film) session.get(Film.class, new Integer(id));
		return film;
	}
	
	public Set<Aktor> getFilmAktor(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Film film = (Film) session.get(Film.class, new Integer(id));
		
		film.getAktor();
		Set<Aktor> listAktor = film.getAktor();
		return listAktor;
	}

	public Film addFilm(Film film) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(film);
		return film;
	}

	public void updateFilm(Film film) {
		this.sessionFactory.getCurrentSession().update(film);
	}

	public void deleteFilm(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Film p = (Film) session.load(Film.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
}
