package org.arpit.java2blog.dao;

import java.util.List;

import org.arpit.java2blog.model.Customer;

public class ResponCustomer {
	private boolean status;
	private String message;
	private List<Customer> list;
	public ResponCustomer() {
		super();
	}
	
	public ResponCustomer(boolean status, String message, List<Customer> list) {
		super();
		this.status = status;
		this.message = message;
		this.list = list;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Customer> getList() {
		return list;
	}
	public void setList(List<Customer> list) {
		this.list = list;
	}
	
}
