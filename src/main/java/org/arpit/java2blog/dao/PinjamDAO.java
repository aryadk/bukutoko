package org.arpit.java2blog.dao;

import java.util.List;

import org.arpit.java2blog.model.Pinjam;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PinjamDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Pinjam> getAllPinjam(){
		Session session = this.sessionFactory.getCurrentSession();
		List<Pinjam> pinjamList = session.createQuery("from Pinjam").list();
		return pinjamList;
	}
	
	public Pinjam getPinjam(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Pinjam pinjam = (Pinjam)session.get(Pinjam.class, new Integer(id));		
		return pinjam;
	}
	
	public List getPinjamByBuku(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Query q = session.createQuery("from Pinjam where buku_id = : id");
		q.setParameter("id", id);
		List pinjamLists = q.list();
		return pinjamLists;
	}
	
	public Pinjam addPinjam(Pinjam pinjam) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pinjam);
		return pinjam;
	}
	
	public void updatePinjam(Pinjam pinjam) {
		this.sessionFactory.getCurrentSession().update(pinjam);
	}
	
	public void deletePinjam(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Pinjam p = (Pinjam) session.load(Pinjam.class, new Integer(id));
		if(null != p) {
			session.delete(p);
		}
	}
}
