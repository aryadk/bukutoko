package org.arpit.java2blog.dao;

import java.util.List;

import org.arpit.java2blog.model.DetailBuku;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DetailBukuDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionfacctory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<DetailBuku> getAllDetailBuku() {
		Session session = this.sessionFactory.getCurrentSession();
		List<DetailBuku> detbukuList = session.createQuery("from DetailBuku").list();
		return detbukuList;
	}

	public DetailBuku getDetailBuku(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		DetailBuku detbuku = (DetailBuku) session.get(DetailBuku.class, new Integer(id));
		return detbuku;
	}

	public DetailBuku addDetailBuku(DetailBuku detbuku) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(detbuku);
		return detbuku;
	}

	public void updateDetailBuku(DetailBuku detbuku) {
		this.sessionFactory.getCurrentSession().update(detbuku);
	}

	public void deleteDetailBuku(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		DetailBuku p = (DetailBuku) session.load(DetailBuku.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
}
