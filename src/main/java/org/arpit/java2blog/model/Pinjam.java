package org.arpit.java2blog.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_pinjam")
public class Pinjam {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column
	String tgl;
	
	@Column
	String nama;
	
	@ManyToOne(targetEntity=Buku.class,cascade = CascadeType.ALL)
	@JoinColumn(name="buku_id")
	Buku buku;
	
	
	public Pinjam() {
		super();
	}

	public Pinjam(int id, String tgl, String nama, Buku buku) {
		super();
		this.id = id;
		this.tgl = tgl;
		this.nama = nama;
		this.buku = buku;
	}

	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	public Buku getBuku() {
		return buku;
	}
	public void setBuku(Buku buku) {
		this.buku = buku;
	}
	
	
}
