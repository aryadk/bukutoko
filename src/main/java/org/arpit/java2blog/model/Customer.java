package org.arpit.java2blog.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="customer_id")
	private int id;
	@Column(name="nama")
	private String nama;
	@Column(name="namaPanjang")
	private String namaPanjang;
	@Column(name="alamat")
	private String alamat;
	@Column(name="umur")
	private int umur;
	@Column(name="status")
	private String status;
	@Column(name="wargaNegara")
	private String wargaNegara;
	@OneToMany(fetch = FetchType.EAGER,cascade= CascadeType.ALL)
	@JoinColumn(name="customer_id")
	private Set<Pinjam> pinjam;
	
	
	public Customer() {
		super();
	}


	public Customer(int id, String nama, String namaPanjang, String alamat, int umur, String status,
			String wargaNegara, Set<Pinjam> pinjam) {
		super();
		this.id = id;
		this.nama = nama;
		this.namaPanjang = namaPanjang;
		this.alamat = alamat;
		this.umur = umur;
		this.status = status;
		this.wargaNegara = wargaNegara;
		this.pinjam = pinjam;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNama() {
		return nama;
	}


	public void setNama(String nama) {
		this.nama = nama;
	}


	public String getNamaPanjang() {
		return namaPanjang;
	}


	public void setNamaPanjang(String namaPanjang) {
		this.namaPanjang = namaPanjang;
	}


	public String getAlamat() {
		return alamat;
	}


	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}


	public int getUmur() {
		return umur;
	}


	public void setUmur(int umur) {
		this.umur = umur;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getWargaNegara() {
		return wargaNegara;
	}


	public void setWargaNegara(String wargaNegara) {
		this.wargaNegara = wargaNegara;
	}


	public Set<Pinjam> getPinjam() {
		return pinjam;
	}


	public void setPinjam(Set<Pinjam> pinjam) {
		this.pinjam = pinjam;
	}
	
	
	
}
