package org.arpit.java2blog.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity 
@Table(name="film")
public class Film {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="film_id")
	private int id;
	@Column
	private String judulFilm;
	@Column
	private int durasi;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinTable(
			name = "film_aktor",
			joinColumns = {@JoinColumn(name="film_id")},
			inverseJoinColumns = {@JoinColumn(name="aktor_id")}
			)
		
	Set<Aktor> aktor = new HashSet<>();
	public Film() {
		super();
	}
	public Film(int id, String judulFilm, int durasi, Set<Aktor> aktor) {
		super();
		this.id = id;
		this.judulFilm = judulFilm;
		this.durasi = durasi;
		this.aktor = aktor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getJudulFilm() {
		return judulFilm;
	}
	public void setJudulFilm(String judulFilm) {
		this.judulFilm = judulFilm;
	}
	public int getDurasi() {
		return durasi;
	}
	public void setDurasi(int durasi) {
		this.durasi = durasi;
	}
	public Set<Aktor> getAktor() {
		return aktor;
	}
	public void setAktor(Set<Aktor> aktor) {
		this.aktor = aktor;
	}
	
	
	
}
