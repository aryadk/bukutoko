package org.arpit.java2blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_detail")
public class DetailBuku {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column
	String pengarang;
	@Column
	String penerbit;
	@Column
	String tahun_terbit;
	/*@OneToOne(targetEntity = Buku.class)
	Buku buku;*/
	
	/*public Buku getBuku() {
		return buku;
	}
	public void setBuku(Buku buku) {
		this.buku = buku;
	}*/
	public DetailBuku() {
		super();
	}
	public DetailBuku(int id,String pengarang,String penerbit,String tahun_terbit) {
		super();
		this.id = id;
		this.penerbit = penerbit;
		this.pengarang = pengarang;
		this.tahun_terbit = tahun_terbit;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPengarang() {
		return pengarang;
	}
	public void setPengarang(String pengarang) {
		this.pengarang = pengarang;
	}
	public String getPenerbit() {
		return penerbit;
	}
	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}
	public String getTahun_terbit() {
		return tahun_terbit;
	}
	public void setTahun_terbit(String tahun_terbit) {
		this.tahun_terbit = tahun_terbit;
	}
}
