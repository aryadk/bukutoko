package org.arpit.java2blog.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "tbl_buku")
public class Buku {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@PrimaryKeyJoinColumn
	int id;
	@Column(name="judul")
	String judul;
	@Column(name="harga")
	int harga;
	@OneToOne(targetEntity=DetailBuku.class,cascade = CascadeType.ALL)
	@JoinColumn(name="detailBuku_id",referencedColumnName="id")
	DetailBuku detailBuku;
	
	/*@OneToMany(targetEntity=Pinjam.class,cascade = CascadeType.ALL)
	@JoinColumn(name="buku_id",referencedColumnName="id")
	private List<Pinjam> pinjam;*/
	
	public Buku() {
		super();
	}
	public Buku(int i, String judul,int harga,DetailBuku detailBuku) {
		super();
		this.id = i;
		this.judul = judul;
		this.harga=harga;
		this.detailBuku = detailBuku;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public int getHarga() {
		return harga;
	}
	public void setHarga(int harga) {
		this.harga = harga;
	}
	public DetailBuku getDetailBuku() {
		return detailBuku;
	}
	public void setDetailBuku(DetailBuku detailBuku) {
		this.detailBuku = detailBuku;
	}
	/*public List<Pinjam> getPinjam() {
		return pinjam;
	}
	public void setPinjam(List<Pinjam> pinjam) {
		this.pinjam = pinjam;
	}*/
	
}
