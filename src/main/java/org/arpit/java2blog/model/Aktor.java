package org.arpit.java2blog.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity 
@Table(name="aktor")
public class Aktor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String namaAktor;
	@Column
	private int umur;
	
	/*@ManyToMany(fetch = FetchType.EAGER,mappedBy = "aktor")
	private Set<Film> film = null; */
	public Aktor() {
		super();
	}

	public Aktor(int id, String namaAktor, int umur) {
		super();
		this.id = id;
		this.namaAktor = namaAktor;
		this.umur = umur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNamaAktor() {
		return namaAktor;
	}

	public void setNamaAktor(String namaAktor) {
		this.namaAktor = namaAktor;
	}

	public int getUmur() {
		return umur;
	}

	public void setUmur(int umur) {
		this.umur = umur;
	}

/*	public Set<Film> getFilm() {
		return film;
	}

	public void setFilm(Set<Film> film) {
		this.film = film;
	}*/
	
	
	
}
